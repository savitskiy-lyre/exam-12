import * as React from 'react';
import {useEffect, useState} from 'react';
import Box from '@mui/material/Box';
import Modal from '@mui/material/Modal';
import {Avatar, Stack, TextField} from "@mui/material";
import AddPhotoAlternateIcon from '@mui/icons-material/AddPhotoAlternate';
import {pink} from "@mui/material/colors";
import Typography from "@mui/material/Typography";
import {LoadingButton} from "@mui/lab";
import {useDispatch} from "react-redux";
import {addPhotoFailure} from "../../store/actions/galleryActions";

const style = {
  position: 'absolute',
  top: '50%',
  left: '50%',
  transform: 'translate(-50%, -50%)',
  width: '500px',
  bgcolor: 'background.paper',
  border: '2px solid #000',
  boxShadow: 24,
  p: 1,
};

const initState = {
  title: '',
  image: null,
}

const AddPhotoModalForm = ({isOpen, closeHandler, onAddSubmit, loading, errs}) => {
  const dispatch = useDispatch();
  const [photoState, setPhotoState] = useState(initState);

  const onPhotoSubmit = (e) => {
    e.preventDefault();

    const photoFormData = new FormData();
    Object.keys(photoState).forEach(key => {
      photoFormData.append(key, photoState[key]);
    })
    const successCallback = () => {
      setPhotoState(initState);
      closeHandler();
    }

    onAddSubmit({data: photoFormData, successCallback});
  }

  const inpImageChanger = (e) => {
    setPhotoState(prev => ({...prev, image: e.target.files[0]}));
  }


  const inpChanger = (e) => {
    setPhotoState(prev => ({...prev, [e.target.name]: e.target.value}));
  }

  useEffect(() => {
    if (errs) {
      if (photoState.title) {
        dispatch(addPhotoFailure({...errs, title: null}));
      }
      if (photoState.image) {
        dispatch(addPhotoFailure({...errs, image: null}));
      }
    }
  }, [photoState])

  return (
    <div>
      <Modal
        open={isOpen}
        onClose={closeHandler}
      >
        <Box sx={style}>
          <Stack component={"form"}
                 noValidate
                 onSubmit={onPhotoSubmit}
                 spacing={2}
          >
            <Avatar sx={{bgcolor: pink[500], padding: '25px', margin: '0 auto'}}>
              <AddPhotoAlternateIcon fontSize={"large"}/>
            </Avatar>
            <Typography variant="h4" component="h2" textAlign={"center"}>
              Add new photo
            </Typography>
            <TextField
              type={"text"}
              name={'title'}
              label={"Title"}
              onChange={inpChanger}
              value={photoState.title}
              helperText={errs?.title?.message}
              error={Boolean(errs?.title)}
            />
            <TextField
              type={"file"}
              name={'image'}
              onChange={inpImageChanger}
              helperText={errs?.image?.message}
              error={Boolean(errs?.image)}
            />
            <LoadingButton type={'submit'}
                           loading={loading}
                           variant={"contained"}
                           color={'success'}
            >
              Submit
            </LoadingButton>
          </Stack>
        </Box>
      </Modal>
    </div>
  );
};

export default AddPhotoModalForm;