import * as React from 'react';
import Card from '@mui/material/Card';
import CardMedia from '@mui/material/CardMedia';
import Typography from '@mui/material/Typography';
import {CardActionArea, Stack} from '@mui/material';

const ActionCard = ({children, image, title, clickHandler}) => (
  <Card sx={{minWidth: 200,maxWidth: 280,}}>
    <CardActionArea onClick={clickHandler}>
      <CardMedia
        component="img"
        height="340"
        image={image}
        alt={title}
      />
      <Stack p={1}>
        <Typography variant="h5" component="div">
          {title}
        </Typography>
      </Stack>
    </CardActionArea>
    {children}
  </Card>
);

export default ActionCard;