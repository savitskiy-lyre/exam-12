import * as React from 'react';
import Box from '@mui/material/Box';
import Modal from '@mui/material/Modal';
import CardMedia from "@mui/material/CardMedia";
import {IMAGES_URL} from "../../../config";

const style = {
  position: 'absolute',
  top: '50%',
  left: '50%',
  transform: 'translate(-50%, -50%)',
  width: '80vw',
  height: '80vh',
  bgcolor: 'background.paper',
  border: '2px solid #000',
  boxShadow: 24,
};

const PhotoModal = ({image, isOpen, closeHandler}) => {
  return (
    <div>
      <Modal
        open={isOpen}
        onClose={closeHandler}
      >
        <Box sx={style}>
          <CardMedia
            component="img"
            height="100%"
            width="100%"
            image={IMAGES_URL + image}
            alt='pic'
          />
        </Box>
      </Modal>
    </div>
  );
};

export default PhotoModal;