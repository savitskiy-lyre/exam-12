import {combineReducers} from "redux";
import {loadFromLocalStorage, saveToLocalStorage} from "./localStorage";
import createSagaMiddleware from 'redux-saga';
import thunk from "redux-thunk";
import {configureStore} from "@reduxjs/toolkit";
import usersSlice from "./slices/usersSlice";
import {rootSagas} from "./rootSagas";
import gallerySlice from "./slices/gallerySlice";

const rootReducer = combineReducers({
    profile: usersSlice.reducer,
    gallery: gallerySlice.reducer,
});
const persistedState = loadFromLocalStorage();

const sagaMiddleware = createSagaMiddleware();

const middleware = [
    thunk,
    sagaMiddleware,
];

const store = configureStore({
    reducer: rootReducer,
    middleware,
    devTools: true,
    preloadedState: persistedState
});

store.subscribe(() => {
    saveToLocalStorage({
        profile: store.getState().profile,
    });
})

sagaMiddleware.run(rootSagas);

export default store;