import {all} from 'redux-saga/effects';
import usersSagas from "./sagas/usersSagas";
import gallerySagas from "./sagas/gallerySagas";

export function* rootSagas() {
  yield all([
    ...usersSagas,
    ...gallerySagas,
  ])
}