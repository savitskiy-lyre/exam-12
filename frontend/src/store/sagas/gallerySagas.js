import {put, takeEvery} from "redux-saga/effects";
import {
  addPhoto,
  addPhotoFailure,
  addPhotoSuccess,
  deletePhoto,
  deletePhotoFailure,
  deletePhotoSuccess,
  fetchGallery,
  fetchGalleryFailure,
  fetchGallerySuccess,
  fetchOwnGallery,
  fetchOwnGalleryFailure,
  fetchOwnGallerySuccess
} from "../actions/galleryActions";
import axiosApi from "../../axiosApi";
import {GALLERY_URL, OWN_GALLERY_URL} from "../../config";
import {toast} from "react-toastify";

export function* fetchGallerySaga() {
  try {
    const {data} = yield axiosApi.get(GALLERY_URL);
    yield put(fetchGallerySuccess(data));
  } catch (err) {
    if (!err.response) toast.error(err.message);
    yield put(fetchGalleryFailure(err.response?.data));
  }
}

export function* fetchOwnGallerySaga({payload}) {
  try {
    const {data} = yield axiosApi.get(OWN_GALLERY_URL + payload);
    yield put(fetchOwnGallerySuccess(data));
  } catch (err) {
    if (!err.response) toast.error(err.message);
    yield put(fetchOwnGalleryFailure(err.response?.data));
  }
}

export function* addPhotoSaga({payload}) {
  try {
    const {data} = yield axiosApi.post(GALLERY_URL, payload.data);
    yield put(addPhotoSuccess(data));
    payload.successCallback();
    toast.success("Created successful !");
  } catch (err) {
    if (!err.response) toast.error(err.message);
    yield put(addPhotoFailure(err.response?.data));
  }
}

export function* deletePhotoSaga({payload: cocktailId}) {
  try {
    yield axiosApi.delete(GALLERY_URL + '/' + cocktailId);
    yield put(deletePhotoSuccess(cocktailId));
    toast.success("Deleted successfully !");
  } catch (err) {
    if (!err.response) {
      toast.error(err.message);
    } else {
      toast.error(err.response.data?.global);
    }
    yield put(deletePhotoFailure(err.response?.data));
  }
}

const usersSaga = [
  takeEvery(fetchGallery, fetchGallerySaga),
  takeEvery(fetchOwnGallery, fetchOwnGallerySaga),
  takeEvery(addPhoto, addPhotoSaga),
  takeEvery(deletePhoto, deletePhotoSaga),
];

export default usersSaga;