import {createSlice} from "@reduxjs/toolkit";

export const initialState = {
  data: null,
  ownGallery: null,
  currentImage: null,
  loadingAdd: false,
  loadingDel: false,
  errFetch: null,
  errFetchOwn: null,
  errAdd: null,
  errDel: null,
};

const name = 'gallery';

const gallerySlice = createSlice({
  name,
  initialState,
  reducers: {
    setCurrentImage(state, {payload}) {
      state.currentImage = payload;
    },
    fetchGallery(state) {
      state.errFetch = null;
    },
    fetchGallerySuccess(state, {payload}) {
      state.data = payload;
    },
    fetchGalleryFailure(state, {payload}) {
      state.errFetch = payload;
    },
    fetchOwnGallery(state) {
      state.errFetchOwn = null;
    },
    fetchOwnGallerySuccess(state, {payload}) {
      state.ownGallery = payload;
    },
    fetchOwnGalleryFailure(state, {payload}) {
      state.errFetchOwn = payload;
    },
    addPhoto(state) {
      state.errAdd = null;
      state.loadingAdd = true;
    },
    addPhotoSuccess(state, {payload}) {
      if (state.ownGallery) {
        state.ownGallery = [...state.ownGallery, payload];
      } else {
        state.ownGallery = [payload];
      }
      state.loadingAdd = false;
    },
    addPhotoFailure(state, {payload}) {
      state.errAdd = payload;
      state.loadingAdd = false;
    },
    deletePhoto(state, {payload}) {
      state.errDel = null;
      state.loadingDel = payload;
    },
    deletePhotoSuccess(state, {payload: cocktailId}) {
      state.ownGallery = state.ownGallery.filter((photo)=>{
        return photo._id !== cocktailId;
      });
      state.loadingDel = false;
    },
    deletePhotoFailure(state, {payload}) {
      state.errDel = payload;
      state.loadingDel = false;
    },
  }
});

export default gallerySlice;