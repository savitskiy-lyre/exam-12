import gallerySlice from "../slices/gallerySlice";

export const {
  setCurrentImage,
  fetchGallery,
  fetchGallerySuccess,
  fetchGalleryFailure,
  fetchOwnGallery,
  fetchOwnGallerySuccess,
  fetchOwnGalleryFailure,
  addPhoto,
  addPhotoSuccess,
  addPhotoFailure,
  deletePhoto,
  deletePhotoSuccess,
  deletePhotoFailure,

} = gallerySlice.actions;