import Layout from "./components/UI/Layout/Layout";
import {Route, Switch} from "react-router-dom";
import SignIn from "./containers/SignIn";
import SignUp from "./containers/SignUp";
import PhotoGallery from "./containers/PhotoGallery/PhotoGallery";
import PersonGallery from "./containers/PersonGallery/PersonGallery";

const App = () => {
    return (
        <Layout>
            <Switch>
                <Route path="/" exact component={PhotoGallery}/>
                <Route path="/gallery/:id"  component={PersonGallery}/>
                <Route path="/signin" component={SignIn}/>
                <Route path="/signup" component={SignUp}/>
            </Switch>
        </Layout>
    );
};

export default App;
