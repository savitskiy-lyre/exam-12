import React, {useEffect, useState} from 'react';
import {Button, Grid, Stack} from "@mui/material";
import {useDispatch, useSelector} from "react-redux";
import ActionCard from "../../components/UI/ActionCard/ActionCard";
import {Link} from "react-router-dom";
import PhotoModal from "../../components/UI/PhotoModal/PhotoModal";
import {fetchGallery, setCurrentImage} from "../../store/actions/galleryActions";
import {GALLERY_LOCATION, IMAGES_URL} from "../../config";
import EmptyPageSkeleton from "../../components/UI/EmptyPageSkeleton/EmptyPageSkeleton";

const PhotoGallery = () => {
  const dispatch = useDispatch();
  const gallery = useSelector(state => state.gallery.data);
  const currentImage = useSelector(state => state.gallery.currentImage);

  const [isCurrentImage, setIsCurrentImage] = useState(false);
  const handleOpenCurrentImage = () => setIsCurrentImage(true);
  const handleCloseCurrentImage = () => setIsCurrentImage(false);

  useEffect(()=>{
    dispatch(fetchGallery());
  },[dispatch])

  return (
    <Stack my={3} flexGrow={1}>
      {!gallery && (
        <EmptyPageSkeleton/>
      )}
      <Grid container spacing={1} flexGrow={1} alignContent={"flex-start"} justifyContent={"center"}>
        {gallery?.length > 0 && (
          gallery.map((photo) => {
            return (
              <Grid item key={photo._id}>
                <ActionCard
                  clickHandler={() => {
                    dispatch(setCurrentImage(photo.image));
                    handleOpenCurrentImage();
                  }}
                  image={IMAGES_URL + photo.image}
                  title={photo.title}
                >
                  <Grid container p={1} justifyContent={"space-between"} spacing={1} alignItems={"center"}>
                    <Grid item>
                      <strong>Created by:</strong>
                    </Grid>
                    <Grid item>
                      <Button variant={'contained'}
                              component={Link}
                              to={GALLERY_LOCATION + photo.author._id}
                      >
                        {photo.author.username}
                      </Button>
                    </Grid>
                  </Grid>
                </ActionCard>
              </Grid>
            )
          })
        )}
      </Grid>
      {currentImage && (
        <PhotoModal
          isOpen={isCurrentImage}
          closeHandler={handleCloseCurrentImage}
          image={currentImage}
        />
      )}
    </Stack>
  );
};

export default PhotoGallery;