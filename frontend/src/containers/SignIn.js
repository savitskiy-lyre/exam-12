import React, {useEffect} from 'react';
import FormRegister from "../components/UI/Form/FormRegister";
import Box from "@mui/material/Box";
import {useDispatch, useSelector} from "react-redux";
import {signIn, signInFailure} from "../store/actions/usersActions";

const SignIn = () => {
    const errLogIn = useSelector((state) => state.profile.errLogIn);
    const loginLoading = useSelector((state) => state.profile.loginLoading);
    const dispatch = useDispatch();

    useEffect(() => () => {
        dispatch(signInFailure(null));
    }, [dispatch]);

    return (
        <Box justifyContent={"center"} width={'100%'} pt={8}>
            <FormRegister
                actionName='Sign in'
                helperLinkName={"Don't have an account? Sign Up"}
                toLocation='/signup'
                loading={loginLoading}
                errors={errLogIn}
                onSubmit={(data) => dispatch(signIn(data))}
            />
        </Box>
    );
};

export default SignIn;