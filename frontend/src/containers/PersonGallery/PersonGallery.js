import React, {useEffect, useState} from 'react';
import {Button, Grid, Stack, Typography} from "@mui/material";
import AddPhotoModalForm from "../../components/AddPhotoModalForm/AddPhotoModalForm";
import {useDispatch, useSelector} from "react-redux";
import {useParams} from "react-router-dom";
import {addPhoto, deletePhoto, fetchOwnGallery, setCurrentImage} from "../../store/actions/galleryActions";
import {IMAGES_URL} from "../../config";
import ActionCard from "../../components/UI/ActionCard/ActionCard";
import PhotoModal from "../../components/UI/PhotoModal/PhotoModal";
import EmptyPageSkeleton from "../../components/UI/EmptyPageSkeleton/EmptyPageSkeleton";
import {LoadingButton} from "@mui/lab";

const PersonGallery = () => {
  const dispatch = useDispatch();
  const params = useParams();
  const profile = useSelector(state => state.profile.data);
  const ownGallery = useSelector(state => state.gallery.ownGallery);
  const currentImage = useSelector(state => state.gallery.currentImage);
  const loadingAdd = useSelector(state => state.gallery.loadingAdd);
  const loadingDel = useSelector(state => state.gallery.loadingDel);
  const formErr = useSelector(state => state.gallery.errAdd);

  const [isCurrentImage, setIsCurrentImage] = useState(false);
  const handleOpenCurrentImage = () => setIsCurrentImage(true);
  const handleCloseCurrentImage = () => setIsCurrentImage(false);

  const [isAddModalOpen, setIsAddModalOpen] = useState(false);
  const handleOpenAddModal = () => setIsAddModalOpen(true);
  const handleCloseAddModal = () => setIsAddModalOpen(false);

  const onAddSubmit = (data) => {
    dispatch(addPhoto(data))
  }

  const onPhotoDelete = (id) => {
    dispatch(deletePhoto(id));
  };

  useEffect(() => {
    dispatch(fetchOwnGallery(params.id));
  }, [dispatch, params.id])

  return (
    <Stack my={3} spacing={2}>
      {profile?._id === params.id && (
        <>
          <Button variant={"contained"}
                  color={"success"}
                  onClick={handleOpenAddModal}
          >
            Add new photo
          </Button>
          <AddPhotoModalForm
            errs={formErr}
            loading={loadingAdd}
            onAddSubmit={onAddSubmit}
            closeHandler={handleCloseAddModal}
            isOpen={isAddModalOpen}
          />
        </>
      )
      }
      {!ownGallery && (
        <EmptyPageSkeleton/>
      )}
      {ownGallery?.length > 0 && (
        <>
          <Typography variant={"h3"} textAlign={"center"}>
            {ownGallery[0].author.username}'s gallery:
          </Typography>
          <Grid container spacing={1} flexGrow={1} alignContent={"flex-start"} justifyContent={"center"}>
            {ownGallery.map(photo => {
              return (
                <Grid item key={photo._id}>
                  <ActionCard
                    clickHandler={() => {
                      dispatch(setCurrentImage(photo.image));
                      handleOpenCurrentImage();
                    }}
                    image={IMAGES_URL + photo.image}
                    title={photo.title}
                  >
                    {profile?._id === params.id && (
                      <LoadingButton
                        loading={loadingDel === photo._id}
                        color={"error"}
                        variant={"contained"}
                        fullWidth
                        onClick={() => onPhotoDelete(photo._id)}
                      >
                        Delete
                      </LoadingButton>
                    )}
                  </ActionCard>
                </Grid>
              )
            })
            }
          </Grid>
        </>
      )
      }

      <PhotoModal
        isOpen={isCurrentImage}
        closeHandler={handleCloseCurrentImage}
        image={currentImage}
      />

    </Stack>
  );
};

export default PersonGallery;


