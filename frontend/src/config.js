export const BASE_URL = 'http://localhost:9000';
export const USERS_URL = '/users';
export const GALLERY_URL = '/gallery';
export const OWN_GALLERY_URL = '/gallery/self/';
export const USERS_SESSIONS_URL = '/users/sessions';
export const GOOGLE_LOGIN = '/users/googleLogin';
export const USER_FC_LOGIN_URL = '/users/facebookLogin';
export const IMAGES_URL = BASE_URL + '/';

export const GALLERY_LOCATION = '/gallery/';

export const facebookAppId = process.env.REACT_APP_FACEBOOK_APP_ID;
