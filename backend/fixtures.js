const mongoose = require('mongoose');
const {nanoid} = require('nanoid');
const config = require('./config');
const User = require("./models/User");
const Photo = require("./models/Photo");


const run = async () => {
    await mongoose.connect(config.db.testStorageUrl)

    const collections = await mongoose.connection.db.listCollections().toArray();
    for (const collection of collections) {
        await mongoose.connection.db.dropCollection(collection.name);
    }

    const [user1, user2, user3] = await User.create({
        username: 'Ivan I',
        password: '123',
        email: 'Ivan@good.com',
        image: '/fixtures/user.svg',
        token: nanoid(),
    }, {
        username: 'Ivan II',
        password: '123',
        email: 'IvanII@good.com',
        image: '/fixtures/user.svg',
        token: nanoid(),
    }, {
        username: 'Pavel',
        password: '123',
        email: 'Pavel@good.com',
        image: '/fixtures/moder.jpg',
        token: nanoid(),
    })

  await Photo.create({
    author: user1,
    title: 'Tree',
    image: '/fixtures/img1.jpg',
  },{
    author: user1,
    title: 'Japan',
    image: '/fixtures/img2.jpg',
  },{
    author: user1,
    title: 'Mountain',
    image: '/fixtures/img3.jpg',
  },{
    author: user1,
    title: 'Butterfly',
    image: '/fixtures/img4.jpg',
  },{
    author: user2,
    title: 'Cat',
    image: '/fixtures/img5.jpg',
  },{
    author: user2,
    title: 'Apple',
    image: '/fixtures/img6.jpg',
  },{
    author: user2,
    title: 'Green',
    image: '/fixtures/img7.jpg',
  },{
    author: user2,
    title: 'Blossom',
    image: '/fixtures/img8.jpg',
  },{
    author: user3,
    title: 'Field',
    image: '/fixtures/img9.jpg',
  },{
    author: user3,
    title: 'Tree',
    image: '/fixtures/img10.jpg',
  },{
    author: user3,
    title: 'Flowers',
    image: '/fixtures/img11.jpg',
  },{
    author: user3,
    title: 'Nonomiya',
    image: '/fixtures/img12.jpg',
  },
    )

    await mongoose.connection.close();
}
run().catch(console.error)
