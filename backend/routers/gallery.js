const express = require('express');
const authUserToken = require("../middleware/authUserToken");
const multer = require("multer");
const config = require("../config");
const {nanoid} = require("nanoid");
const path = require("path");
const router = express.Router();
const Photo = require('../models/Photo');

const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, config.uploadPath);
  },
  filename: (req, file, cb) => {
    cb(null, nanoid() + path.extname(file.originalname));
  }
});

const upload = multer({storage});

router.get('/', async (req, res) => {
  try {
    const gallery = await Photo.find().populate('author', 'username');
    res.send(gallery);
  } catch (err) {
    res.status(500).send({global: 'Server error, please try again !'});
  }
})

router.get('/self/:id', async (req, res) => {
  const searchId = req.params.id;
  if (!searchId) return res.status(400).send({global: 'Bad request'});
  try {
    if (!searchId) return res.status(403).send({global: 'It looks like your account is corrupted'})
    const gallery = await Photo.find({author: searchId}).populate('author', 'username');
    res.send(gallery);
  } catch (err) {
    res.status(500).send({global: 'Server error, please try again !'});
  }
})

router.post('/', authUserToken, upload.single('image'), async (req, res) => {
  const prePhoto = {title: req.body.title, author: req.user._id};
  if (req.file) prePhoto.image = '/uploads/' + req.file.filename;
  const newPhoto = new Photo(prePhoto);
  try {
    await newPhoto.save();
    const populatedPhoto = await Photo.findById(newPhoto).populate('author', 'username');
    res.send(populatedPhoto);
  } catch (err) {
    if (!err.errors) return res.status(500).send({global: 'Server error, please try again !'});
    res.status(400).send(err.errors);
  }
})

router.delete('/:id', authUserToken, async (req, res) => {
  const id = req.params.id;
  try {
    const photo = await Photo.findById(id);
    if (!photo) return res.status(404).send({global: 'Photo not found'});
    if (!photo.author.equals(req.user._id)) return res.status(403).send({global: 'Permission denied'});
    await Photo.findByIdAndDelete(id);
    res.send({global: 'Deleted successfully'});
  } catch (err) {
    res.status(500).send({global: 'Server error, please try again !'});
  }
})

module.exports = router;