const mongoose = require('mongoose');
const idValidator = require('mongoose-id-validator');

const PhotoSchema = new mongoose.Schema({
  title: {
    type: String,
    required: [true, 'Title is required'],
  },
  image: {
    type: String,
    required: [true, 'Image is required'],
  },
  author: {
    type: mongoose.Schema.Types.ObjectId,
    required: [true, 'Author ID is required'],
    ref: 'users',
  },
})


PhotoSchema.plugin(idValidator);
const Photo = mongoose.model('gallery', PhotoSchema);
module.exports = Photo;